Follow the instructions in the INSTALL.txt file, and then come back.

Initial:

When you right-click on an image in your cafe shop, and click save image as, it
will save it as "12345_F_store" or something similar. Rename the image to just
the number (12345 in this example). Then put them in some folder that your web
server can reach. Fill in the information in the module configuration section,
and you're all set.

Advanced:

You can also turn on and off extra link options back to your cafepress store.
Check out Admin -> Settings -> Cafepress

If you would like to avoid downloading specific images from your store, you can download
cafepressbox from http://www.getfreesofts.com/script/883/8648/CafePress_Random_Product.html
and place the class.cafepressbox.php file in your modules/cafepress directory. It will
get the list of products and images directly from your store.

